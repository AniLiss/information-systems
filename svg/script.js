/**
 * Created by Elizabeth on 23.10.2016.
 */
// Textarea Actions
//----------------------------------------
var doc = document;
var init = doc.querySelector( "#editor" );
var res = doc.querySelector( ".svg-demo" );
var symbols = /[\r\n"%#()<>?\[\\\]^`{|}]/g;



init.onkeyup = function() {
    var namespaced = addNameSpace( init.value );
    var escaped = encodeSVG( namespaced );
    var resultCss = 'background-image: url("data:image/svg+xml,' + escaped + '");';
    res.setAttribute( "style", resultCss );
};
// Namespace
//----------------------------------------

function addNameSpace( data ) {
    if ( data.indexOf( "http://www.w3.org/2000/svg" ) < 0 ) {
        data = data.replace( /<svg/g, "<svg xmlns='http://www.w3.org/2000/svg'" );
    }
    return data;
}

// Encoding
//----------------------------------------

function encodeSVG( data ) {
    // Use single quotes instead of double to avoid encoding.
    if ( data.indexOf( '"' ) >= 0 ) {
        data = data.replace( /"/g, "'" );
    }

    data = data.replace( />\s{1,}</g, "><" );
    data = data.replace( /\s{2,}/g, " " );

    return data.replace( symbols, escape );
}
